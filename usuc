#!/usr/bin/env bash
set -euo pipefail

if [ $# -lt 1 ]; then
    echo "Usage: $0 <channel>"
    exit 1
fi

CHANNEL=$1

# Autowrap self in rlwrap
if [ -z "${RLWRAP:-}" ]
then
    RLWRAP=1 rlwrap "$0" "$@"
    exit 0
fi

# BSD portability
if [ "$(uname)" = "Linux" ]
then
    STATARGS="-c %U"
else
    STATARGS="-f %Su"
fi
chan_owner=$(stat $STATARGS /var/lib/suc/"$CHANNEL")
if [ "$chan_owner" != suc ]
then
    SUC=suc_"$chan_owner"
else
    SUC=suc
fi
# Tail the channel
tail -f -n 20 /var/lib/suc/"$CHANNEL"&
while true
do
    read -r line || exit 0
    if [ "${line::1}" == ":" ]
    then
        echo '*runs* `' "${line:1}" '`' | pygmentize -l md -f 256 | "$SUC" "$CHANNEL"
        bash -c "${line:1}" | "$SUC" "$CHANNEL"
    else
        echo "$line" | fmt | pygmentize -l md -f 256 | "$SUC" "$CHANNEL"
    fi
done
